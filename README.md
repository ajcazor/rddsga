# rddsga
Subgroup analysis with propensity score weighting in RDD settings

# Installation
The program can be installed from the Github mirrored repository using
```stata
net install rddsga, from (https://raw.githubusercontent.com/acarril/rddsga/master/)
```

If you have installed the program and want to update it, be sure to `net install` using options `replace force`:
```stata
net install rddsga, from (https://raw.githubusercontent.com/acarril/rddsga/master/) replace force
```

# Help
Refer to the included help file for details regarding usage.
For additional details regarding the methodology refer to the [project wiki](https://gitlab.com/acarril/rddsga/wikis/home).